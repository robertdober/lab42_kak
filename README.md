
[![Gem Version](https://badge.fury.io/rb/lab42_kak.svg)](http://badge.fury.io/rb/lab42_kak)


# Lab42::KAK

Experiments in scripting [Kakoune](https://github.com/mawww/kakoune)

## Kakoune Scripting

... basically works by calling filters, however a lot of information is exported via Environment Variables
# LICENSE

Copyright 2020 Robert Dober robert.dober@gmail.com

Apache-2.0 [c.f LICENSE](LICENSE)
