require_relative "kak/env"
require_relative "kak/inp"
require_relative "kak/cccomplete"
module Lab42
  class KAK
    include ENV
    include INP
    include CCComplete
  end
end
