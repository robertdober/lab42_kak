require "lab42/open_map/include"
module Lab42
  class KAK
    module INP
      INDENT_RGX = %r{\A\s*}
      def first_line
        @__first_line__ ||= lines.first
      end
      def indent
        @__indent__ ||= first_line[INDENT_RGX]
      end
      def indented what
        case what
        when Enumerable
          what.map{ "#{indent}#{_1}" }
        else
          "#{indent}#{what}" 
        end
      end
      def other_lines
        @__other_lines__ ||= lines[1..-1]
      end
      def lines
        @__lines__ ||= $stdin.readlines(chomp: true)
      end
    end
  end
end

