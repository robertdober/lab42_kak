require "lab42/open_map/include"
module Lab42
  class KAK
    module ENV
      def options
        @__options__ ||= _extract_options
      end
      def registers
        @__registers__ ||= _extract_registers
      end
      def values
        @__values__ ||= _extract_values
      end

      private

      KAK_OPTION_RGX = %r{\Akak_opt_}
      def _extract_options
        _kak_env.keys.grep(KAK_OPTION_RGX).inject(OpenMap.new) do |result, name|
          result.merge(name[8..-1].to_sym => _kak_env[name])
        end
      end

      KAK_REGISTER_RGX = %r{\Akak_reg_}
      def _extract_registers
        _kak_env.keys.grep(KAK_REGISTER_RGX).inject(OpenMap.new) do |result, name|
          result.merge(name[8..-1].to_sym => _kak_env[name])
        end
      end

      KAK_VALUE_RGX = %r{\Akak_(?!(?:opt|reg)_)}
      def _extract_values
        _kak_env.keys.grep(KAK_VALUE_RGX).inject(OpenMap.new) do |result, name|
          result.merge(name[4..-1].to_sym => _kak_env[name])
        end
      end

      def _kak_env; ::ENV end
    end
  end
end

