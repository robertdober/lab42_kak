require_relative "cccomplete/generic"
module Lab42
  class KAK
    module CCComplete
      def cccomplete
        $stderr.puts "================ #{Time.now}"
        case options.filetype
        when "markdown"
          require_relative "cccomplete/markdown"
          Markdown.new(self).markdown_complete
        when "ruby"
          require_relative "cccomplete/ruby"
          Ruby.new(self).ruby_complete
        else
          Generic::NOP
        end
      end
    end
  end
end

