module Lab42
  class KAK
    class Ruby
      include Generic

      ADD_DO_RGX = %r{(?:\s+do\s*)?\z}
      COMPLETE_WO_DO_RGX = %r{\A\s*(?:class|def|module|modulefunction)}

      def ruby_complete
        $stderr.puts "FT Triggered Ruby"
        $stderr.puts "selection: #{_selection.inspect}"
        case _selection
        when COMPLETE_WO_DO_RGX
          _ruby_complete_wo_do
        else
          _ruby_default_complete
        end
        output.flatten.join
      end


      private

      def _ruby_complete_wo_do
        add "dO"
        add _selection, nl: true
        add_lines "  ", "end", indented: true
        add  "<esc>;kA"
      end

      def _ruby_default_complete
        add "dO"
        add _selection.sub(ADD_DO_RGX, " do"), nl: true
        add_lines "  ", "end", indented: true
        add  "<esc>;kA"
      end

    end
  end
end
