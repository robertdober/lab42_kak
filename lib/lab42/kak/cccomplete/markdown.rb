module Lab42
  class KAK
    class Markdown
      include Generic

      RCODE_TRIGGER_RGX = %r{
        \A (?:And,?|But,?|Example:?|Given,?|Then,?) \b
      }x

      def markdown_complete
        $stderr.puts "FT Triggered Markdown"
        case _selection
        when RCODE_TRIGGER_RGX
          _rcode_completion
        else
          NOP
        end
      end

      private

      def _rcode_completion
        add "o"
        add_lines "```ruby", "    ", "```"
        add  "<esc>;kA"
      end
    end
  end
end
