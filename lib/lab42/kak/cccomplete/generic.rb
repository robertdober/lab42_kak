module Lab42
  class KAK
    module Generic
      INDENT_RGX = %r{\A\s*}
      NL         = "<ret>"
      NOP        = ":nop<ret>;"

      attr_reader :kak, :output

      private

      def _add_one value, nl:, indented:
        output << indent(indented)
        output << value
        output << nl(nl)
      end

      def _indent
        @___indent__ ||= _selection[INDENT_RGX]
      end

      def _selection
        @___selection__ ||= kak.values.selection.chomp
      end


      def add *values, nl: false, indented: false
        # p [:values, values]
        values
          .flatten
          .each { _add_one(_1, nl: nl, indented: indented) }
        # p [:output, output]
      end

      def add_lines *values, indented: false
        lines =
          values
            .flatten
            .map { "#{indent(indented)}#{_1}" }
          add lines.join(NL)
      end

      def indent indented
        indented ? _indent : nil
      end

      def nl nl
        nl ? NL : nil
      end

      def initialize kak
        @kak = kak
        @output = []
      end
    end
  end
end
