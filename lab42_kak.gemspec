$:.unshift( File.expand_path( "../lib", __FILE__ ) )
require "lab42/kak/version"
version = Lab42::KAK::VERSION
Gem::Specification.new do |s|
  s.name        = 'lab42_kak'
  s.version     = version
  s.summary     = 'Scripting Kakoune'
  s.description = %{Kakoune scripting is very flexible but needs to be triggered by the shell, quite interesting how that goes...}
  s.authors     = ["Robert Dober"]
  s.email       = 'robert.dober@gmail.com'
  s.files       = Dir.glob("{bin,lib}/**/*.rb")
  s.files      += %w{LICENSE README.md}
  s.executables = Dir.glob("bin/*.rb").map{File.basename _1}
  s.homepage    = "https://bitbucket.org/robertdober/lab42_kak"
  s.licenses    = %w{Apache-2.0}

  s.required_ruby_version = '>= 2.7.0'

  s.add_dependency 'lab42_open_map', '~> 0.1.1'

  s.add_development_dependency 'pry', '~> 0.10'
  s.add_development_dependency 'pry-byebug', '~> 3.9'
  s.add_development_dependency 'rspec', '~> 3.10'
end
