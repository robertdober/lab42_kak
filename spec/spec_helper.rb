
require 'simplecov'
SimpleCov.start do
  # enable_coverage :branch
  add_filter %r{/spec/}
end


$: << File.expand_path("../lib")
require "lab42/kak/autoimport"
PROJECT_ROOT = File.expand_path "../..", __FILE__
Dir[File.join(PROJECT_ROOT,"spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |c|
  c.filter_run wip: true
  c.filter_run_excluding single_md: true
  c.run_all_when_everything_filtered = true

  if c.files_to_run.one?
    # Use the documentation formatter for detailed output,
    # unless a formatter has already been configured
    # (e.g. via a command-line flag).
    c.default_formatter = "doc"
  end

end

