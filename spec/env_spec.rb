RSpec.describe KAK do

  mock_input only_variables: true

  context "simple access to Kakoune's exported environment" do
    set_kak_env(
      options: {autoreload: "ask", makecmd: "make"},
      registers: {slash: "ruby", x: "some text"},
      values: {anything: "some value"}
    )

    subject { described_class.new }

    it "can access the options" do
      expect( subject.options.to_h ).to eq(autoreload: "ask", makecmd: "make")
    end

    it "can access the registers" do
      expect( subject.registers.to_h ).to eq(slash: "ruby", x: "some text")
    end

    it "can access the values" do
      expect( subject.values.to_h ).to eq(anything: "some value", selection: "")
    end

    it "can access options by name" do
      expect( subject.options.autoreload ).to eq("ask")
    end

    it "can access registers by name" do
      expect( subject.registers.slash ).to eq("ruby")
    end

    it "can access values by name" do
      expect( subject.values.anything ).to eq("some value")
    end
  end

end
