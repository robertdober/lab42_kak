RSpec.describe KAK do

  context "ruby filetype" do
    mock_input
    subject { described_class.new }
    let(:options) {{filetype: "ruby"}}

    describe "completions without do" do
      shared_examples_for "completion w/o do" do
        it "only adds two lines" do
          expect( subject.cccomplete ).to eq( input + ["#{indent}  ", "#{indent}end"] )
        end
      end
      context "def" do
        let(:input) { [ "  def hello(world)"] }
        it_behaves_like "completion w/o do"
      end
      context "module" do
        let(:input) { [ "  module hello(world)"] }
        it_behaves_like "completion w/o do"
      end
      context "class" do
        let(:input) { [ "  class hello(world)"] }
        it_behaves_like "completion w/o do"
      end
    end

    private

    def indent
      input.first[%r{\A\s*}]
    end
  end
end
