RSpec.describe KAK do
  RET = "<ret>"
  mock_input
  subject { described_class.new }

  context "ruby filetype" do
    let(:options) {{filetype: "ruby"}}

    describe "a line without a do" do
      let(:selection) {"  some_call(a, b)"}
      let :expected do
        ["dO  some_call(a, b) do", "    ", "  end<esc>;kA"].join(RET)
      end
      it "returns the correct lines" do
        expect(subject.cccomplete).to eq(expected)
      end
    end
    describe "a line with do" do
      let(:selection) {"  some_call(a, b)  do "}
      let :expected do
        ["dO  some_call(a, b) do", "    ", "  end<esc>;kA"].join(RET)
      end
      it "returns the correct lines" do
        expect(subject.cccomplete).to eq(expected)
      end
    end
  end
  context "different filetype" do
    let(:options) {{filetype: "unknown"}}
    describe "Good old dungeon: Wait, ..." do
      let(:selection) {"  some_call(a, b)"}
      it "nothing happens" do
        expect( subject.cccomplete ).to eq(":nop<ret>;")
      end
    end
  end
end
