RSpec.describe KAK do
  context "markdown filetype" do
    mock_input
    subject { described_class.new }
    let(:options) {{filetype: "markdown"}}

    describe "completions without rcode" do
      shared_examples_for "completion w/o rcode" do
        it "'s a NOP" do
          expect( subject.cccomplete ).to eq( input )
        end
      end
      context "def" do
        let(:input) { [ "  Given that"] }
        it_behaves_like "completion w/o rcode"
      end
      context "module" do
        let(:input) { [ "  And"] }
        it_behaves_like "completion w/o rcode"
      end
      context "class" do
        let(:input) { [ "  Then"] }
        it_behaves_like "completion w/o rcode"
      end
    end

    describe "completions with rcode" do
      shared_examples_for "completion with rcode" do
        it "adds a nice ```ruby code block" do
          expect( subject.cccomplete ).to eq( [input, "```ruby", "    ", "```"] )
        end
      end
      context "Given" do
        let(:input) 
      end
    end
  end
end
