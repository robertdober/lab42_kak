module Support
  module EnvSetter extend self

    def set(options:, registers:, selection:, values:)
      values =
      _set_options(options)
        .merge(_set_registers(registers))
        .merge(_set_values(values.merge(selection: selection)))
    end

    private

    def _set_options(options)
      options.inject Hash.new do |hsh, (name, value)|
        hsh.merge("kak_opt_#{name}" => value)
      end
    end

    def _set_registers(registers)
      registers.inject Hash.new do |hsh, (name, value)|
        hsh.merge("kak_reg_#{name}" => value)
      end
    end

    def _set_values(values)
      values.inject Hash.new do |hsh, (name, value)|
        hsh.merge("kak_#{name}" => value)
      end
    end
  end
end
