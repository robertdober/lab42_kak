require_relative "env_setter"
module Support
  module PopulateEnvExample
    private

    def _mock_env values
      allow(ENV).to receive(:[]).and_call_original
      allow(ENV).to receive(:keys).and_return(values.keys)
      values.each {_mock_env_access _1, _2}
    end

    def _mock_env_access key, value
      allow(ENV).to receive(:[]).with(key).and_return(value)
    end
  end

  module PopulateEnv
    def mock_input only_variables: true
      before do
        allow($stderr).to receive(:puts)
        expect($stdin).to receive(:readlines).with(chomp: true).once.and_return(Array(input)) unless only_variables

        kak_env = EnvSetter.set(options: options, registers: registers, selection: selection, values: values)
        _mock_env kak_env
      end
      let(:input) { [] }
      let(:options){ {} }
      let(:registers){ {} }
      let(:selection) { "" }
      let(:values){ {} }
    end

    def set_ruby_env(input: nil, options: nil, values: nil, registers: nil)
      set_kak_env(input: input, options: (options||{}).merge(filetype: "ruby"), registers: registers, values: values)
    end

    def set_kak_env(input: nil, options: nil, values: nil, registers: nil)
      _define_let :options, options
      _define_let :registers, registers
      _define_let :values, values

      if input
        let(:input) {
          input
        }
      end
    end

    def set_kak_selection with
      set_kak_env(values: {selection: with})
    end

    private

    def _define_let name, value
      if value
        let(name) { super().merge(value) }
      end
    end

  end
end

RSpec.configure do |cfg|
  cfg.extend Support::PopulateEnv
  cfg.include Support::PopulateEnvExample
end
